# Single Cell RNA-seq from Somato-Sensory Cortex

This repository contains the differential anaysis of single cell RNA-seq data from somato-sensory cortex.

Final presentation slides can be found [here](./reports/slides.pdf) and edited [here](https://docs.google.com/presentation/d/17oFoacEgXD2PJ7BsflozFydtiDj0q9-pkXPLvXhKMkQ/edit?usp=sharing).


## Data

Data, as Seurat object, available in this [PanglaoDB page](https://panglaodb.se/view_data.php?sra=SRA667466&srs=SRS3060049).


## Dependencies

Dependencies are managed in a `renv` virtual environment. For more info, check out its [web page](https://rstudio.github.io/renv/).

To install all dependencies, saved in the `renv.lock` file, run in the R console:

    install.packages("renv")
    renv::init()

Then select the option for restoring the project from the lock file.

`renv` functionalities are also integrated into [RStudio IDE](https://docs.posit.co/ide/user/ide/guide/environments/r/renv.html) project options.


## Pipeline

These are the analysis steps:

1. Data wrangling
2. Quality control and filter
3. Normalization
4. Scaling
5. Dimensionality reduction
6. Clustering
7. Plot UMAP and TSNE
8. Find marker genes
9. Label clusters by marker genes
